import java.util.Stack;


/*
* I convert input string to reverse polish notation and count expression using stack.
*/
public class Main {
    public Stack<String> translationStack;
    public static Stack<Double> stackCounter;
    private String inputString;
    private String resultString;
    private String stringRPN;

    Main(String inputStr) {
        stackCounter = new Stack<Double>();
        translationStack = new Stack<String>();
        inputString = inputStr;
        resultString = inputStr;
        stringRPN = "";
    };

    public String getResultString() {
        return resultString;
    }

    public void run() {
        try {
            translateToRPN(inputString);
            resultString += " = " + countRes();
            System.out.println(resultString);
        } catch (Exception e) {
            System.err.println(e);
            System.exit(-1);
        }
    }


    private double countRes() throws Exception {
        String[] elements = stringRPN.split(" ");
        for (int i = 0; i < elements.length; i++) {
            if (isNum(elements[i])) {
                stackCounter.push(Double.parseDouble(elements[i]));
            } else if (isBinaryOperand(elements[i])) {
                stackCounter.push(makeBinaryOperation(elements[i]));
            } else {
                stackCounter.push(makeUnaryOperation(elements[i]));
            }
        }
        return stackCounter.pop();
    }

    private static Double makeUnaryOperation(String operator) throws Exception {
        double a = stackCounter.pop();
        if (operator.equals("sin")) {
            return Math.sin(toRadians(a));
        } else if (operator.equals("cos")) {
            return Math.cos(toRadians(a));
        } else if (operator.equals("tg")) {
            return Math.tan(toRadians(a));
        } else if (operator.equals("sqrt")) {
            return Math.sqrt(a);
        } else if (operator.equals("log")) {
            if (a == 0)
                throw new Exception("Wrong expression");
            return Math.log(a);
        } else throw new Exception("Wrong expression!");
    }

    private static double toRadians(double deg) {
        deg = deg * (Math.PI / 180);
        return deg;
    }

    private static Double makeBinaryOperation(String operation) throws Exception {
        double a = stackCounter.pop();
        double b = stackCounter.pop();
        if (operation.equals("*"))
            return a * b;
        else if (operation.equals("/")) {
            if (a == 0)
                throw new Exception("Division by zero!");
            return b / a;
        } else if (operation.equals("+"))
            return a + b;
        else if (operation.equals("-")) {
            return b - a;
        } else if (operation.equals("^")) {
            return Math.pow(b, a);
        } else
            throw new Exception("Wrong expression!");
    }

    private static boolean isBinaryOperand(String num) {
        if (num.equals("+") || num.equals("-") || num.equals("*") || num.equals("/") || num.equals("^"))
            return true;
        else return false;
    }

    private void translateToRPN(String str) {
        for (int i = 0; i < str.length(); i++) {
            if (isOpenComma(str.charAt(i))) {
                pushOpenComma(str, i);
            } else if (isOperand(String.valueOf(str.charAt(i)))) {
                i = getFullOperandName(str, i);
            } else if (isNum(String.valueOf(str.charAt(i)))) {
                i = addNumsToStringRPN(str, i);
            } else if (isCloseComma(str.charAt(i))) {
                popOperandsBeforeComma();
            }
        }
        while (!translationStack.isEmpty()) {
            stringRPN += translationStack.pop() + " ";
        }
    }

    private void pushOpenComma(String str, int i) {
        translationStack.push(String.valueOf(str.charAt(i)));
    }

    private int getFullOperandName(String str, int i) {
        String operand = getOperand(str.substring(i));
        if (!translationStack.isEmpty() && getPriority(operand) <= getPriority(translationStack.peek()))
            stringRPN = getOperandsWithLessPriority(operand);
        translationStack.push(operand);
        i += operand.length() - 1;
        return i;
    }

    private void popOperandsBeforeComma() {
        while (!translationStack.peek().equals("(")) {
            stringRPN += translationStack.pop() + " ";
        }
        removeOpenComma();
    }

    private void removeOpenComma() {
        translationStack.pop();
    }

    private int addNumsToStringRPN(String str, int i) {
        String number = getNum(str.substring(i));
        stringRPN += number + " ";
        i += number.length() - 1;
        return i;
    }

    private String getOperandsWithLessPriority(String operand) {
        do {
            stringRPN += translationStack.pop() + " ";
        }
        while ((!translationStack.isEmpty()) && (getPriority(operand) <= getPriority(translationStack.peek())));
        return stringRPN;
    }

    private static int getPriority(String operand) {
        if (operand.equals("tg") || operand.equals("sin")
                || operand.equals("cos") || operand.equals("log")
                || operand.equals("sqrt")) {
            return 4;
        } else if (operand.equals("*") || operand.equals("/")) {
            return 3;
        } else if (operand.equals("+") || operand.equals("-")) {
            return 2;
        } else if (operand.equals("(")) {
            return 1;
        } else if (operand.equals("^")) {
            return 5;
        }
        return 0;
    }


    private static String getNum(String string) {
        int i = 0;
        while (Character.isDigit(string.charAt(i)) && i < string.length()) {
            i++;
            if (i == string.length())
                return string.substring(0);
        }
        return string.substring(0, i);
    }

    private static String getOperand(String str) {
        if (Character.isLetter(str.charAt(0))) {
            return (str.substring(0, str.indexOf('(')));
        } else return str.substring(0, 1);
    }

    private static boolean isOpenComma(char c) {
        if (c == '(')
            return true;
        else return false;
    }

    private static boolean isCloseComma(char c) {
        if (c == ')')
            return true;
        else return false;
    }

    private static boolean isNum(String c) {
        return (Character.isDigit(c.charAt(0)));
    }

    private static boolean isOperand(String c) {
        return !Character.isDigit(c.charAt(0)) && !isOpenComma(c.charAt(0)) && !isCloseComma(c.charAt(0));
    }
}
