import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

/*
* Create tests
*/
public class TestClass extends TestCase {
    public TestClass(String testName) {
        super(testName);
    }

    public void test1() {
        Main t1 = new Main("5+6");
        t1.run();
        assertTrue(t1.getResultString().equals("5+6 = 11.0"));
    }

    public void test2() {
        Main t2 = new Main(" sin(42)");
        t2.run();
        assertTrue(t2.getResultString().equals(" sin(42) = 0.6691306063588582"));
    }

    public void test3() {
        Main t3 = new Main("(57+1/tg(53))*sqrt(54-cos(23))");
        t3.run();
        assertTrue(t3.getResultString().equals("(57+1/tg(53))*sqrt(54-cos(23)) = 420.76742174571075"));
    }

    public void test4() {
        Main t3 = new Main("5^(2*2)");
        t3.run();
        assertTrue(t3.getResultString().equals("5^(2*2) = 625.0"));
    }
}

class Go {
    public static void main(String[] args) {
        TestRunner runner = new TestRunner();
        TestSuite suite = new TestSuite();
        suite.addTest(new TestClass("test1"));
        suite.addTest(new TestClass("test2"));
        suite.addTest(new TestClass("test3"));
        suite.addTest(new TestClass("test4"));
        runner.doRun(suite);
    }
}
